const router = require('express').Router()
const alunoController = require('../controller/alunoController')
const teacherController = require('../controller/teacherController')
const JSONPlaceholderController = require('../controller/JSONPlaceholderController')


router.get('/teacher/', teacherController.getTeacher);
router.post('/aluno/', alunoController.postAluno);
router.put('/aluno/', alunoController.updateAluno);
router.delete('/aluno/:id', alunoController.deleteAluno);

router.get("/external/", JSONPlaceholderController.getUsers);
router.get("/external/io", JSONPlaceholderController.getUsersWebSiteIO);
router.get("/external/com", JSONPlaceholderController.getUsersWebSiteCOM);
router.get("/external/net", JSONPlaceholderController.getUsersWebSiteNET);
router.get('/external/filter', JSONPlaceholderController.getCountDomain);
module.exports = router